package com.meru.PaymentService.service.impl;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meru.PaymentService.external.model.Order;
import com.meru.PaymentService.service.PaymentService;
@Service
public class PaymentServiceImpl implements PaymentService{

	@Override
	public String doPayment(String jsonOrder) {
		ObjectMapper om = new ObjectMapper();
		String updatedOrderJson = null;
		
		try {
			Order order = om.readValue(jsonOrder, Order.class);
			System.out.println("Total amount paid = "+order.getOrderAmount());
			order.setPaymentConfirmed(true);
			updatedOrderJson  = om.writeValueAsString(order);
			System.out.println(updatedOrderJson);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return updatedOrderJson;
	}

	
}

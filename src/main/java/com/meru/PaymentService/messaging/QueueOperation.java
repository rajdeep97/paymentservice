package com.meru.PaymentService.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import com.meru.PaymentService.service.PaymentService;
@Component
public class QueueOperation {

	public static final String PAYMENT_REQUESTED = "order.payment.REQUEST";
	public static final String PAYMENT_SUCCESSFUL = "order.payment.SUCCESS";
	
	@Autowired
	private PaymentService paymentService;
	
	@JmsListener(destination = PAYMENT_REQUESTED)
	@SendTo(PAYMENT_SUCCESSFUL)
	public String doPayment(String jsonOrder)
	{
		return this.paymentService.doPayment(jsonOrder);
	}
}
